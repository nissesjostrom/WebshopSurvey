#-!- encoding: utf-8 -!-
#Author: Oscar Arnflo
#Copyright (c) Säkerhetskontoret i Sverige AB 2016

from reportlib import Report
from reportlib import platypus
from reportlab.platypus import Table
import json
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.platypus import Table as pdfTable
import binascii

class PDF:
    """ Create a PDF-report for a website scan. """

    def __init__(self,data,filename="scan-report.pdf"):
        """ Init. report, set data to be presented.
        @param data JSON-data to be presented
        @param filename filename to store the report
        """
        self.data = json.loads(data)
        self.filename = filename
        self._build()

    def _build(self):
        """ Build PDF-report. """
        pdf = Report.Report(self.filename+".pdf")
        pdf.add_logo()
        header = "Webscan results <br/><br/> %s" % self.filename
        pdf.add_flowable(platypus.Separator(header )) #Add separator with cool name ;)

        #Build result table
        data = [ (key, val) for key, val in self.data.iteritems() ]
        table = Table(data, colWidths=220, style=[ ('ALIGN',(1,0),(1,-1),'RIGHT') ])
        print table
        #TODO: even-odd color?
        pdf.add_flowable(table)


        pdf.save()
        print "Report saved to", self.filename
