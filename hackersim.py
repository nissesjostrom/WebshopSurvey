import time
import random

COLORS = ['\033[94m','\033[95m', '\033[93m', '\033[92m', '\033[91m','\033[36m','\033[35m','\033[31m']

for i in range(100):
	print "".join([ COLORS[random.randint(0,len(COLORS)-1)] + chr(random.randint(0,255)) for j in range(200) ])
	time.sleep(0.05)
