#-!- encoding: utf-8 -!-
#Author: Oscar Arnflo 2016
#Copyright (c) Säkerhetskontoret i Sverige AB 2016

import os
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Image, Frame
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlib import platypus

class Report:
    """
    Creates a report.
    """

    def __init__(self,filename="report.pdf"):
        self.width = 460
        self.margin = 72
        self.bottom = 60
        self.top = 20

        self.doc = SimpleDocTemplate(filename,pagesize=letter,
        rightMargin=self.margin, leftMargin=self.margin,topMargin=self.top,
        bottomMargin=self.bottom)

        self.flow = []

        #Register fonts
        pdfmetrics.registerFont(TTFont("Titillium-title",
        os.path.dirname(__file__)+"/resources/TitilliumText25L-800wt.ttf"))
        pdfmetrics.registerFont(TTFont("Titillium-ingress",
        os.path.dirname(__file__)+"/resources/TitilliumText25L-250wt.ttf"))
        pdfmetrics.registerFont(TTFont("New Century",
        os.path.dirname(__file__)+"/resources/NewCenturySchlbkLTStd-Roman.ttf"))

    def add_logo(self):
        """Add säkerhetskontorets logo to the report"""
        width = 1161.0
        height = 291.0
        nwidth = 6
        nheight = nwidth*height/width
        self.flow.append(Image(os.path.dirname(__file__)+
        "/resources/Logo_liggande.jpg", width=nwidth*inch, height=nheight*inch))

    def add_flowable(self,flowable):
        """Add Flowable to report."""
        self.flow.append(flowable)

    def save(self):
        """Build and save report."""
        self.doc.build(self.flow, onFirstPage=self._footer, onLaterPages=self._footer)

    def _footer(self,canvas,doc):
        flows = [platypus.Separator()]
        text = """<font name='Titillium-ingress'>
        Säkerhetskontoret i Sverige AB<br/>
        Sandelsgatan 12, 115 34 Stockholm<br/>
        www.sakerhetskontoret.com
        </font>"""
        flows.append(Paragraph(text, getSampleStyleSheet()["BodyText"]))

        Frame(self.margin,0,460,self.bottom).addFromList(flows,canvas)
