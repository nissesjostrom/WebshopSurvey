#-!- encoding: utf-8 -!-
#Author: Oscar Arnflo 2016
#Copyright (c) Säkerhetskontoret i Sverige AB 2016

from reportlab.platypus import Flowable, Paragraph
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlib import style

class Separator(Flowable):
    """Make a clean separator with title."""

    def __init__(self,text=None):
        Flowable.__init__(self)
        self.text = text
        self.height = 0 if text == None else 10+0.3*inch

    def draw(self):
        if self.text != None:
            self.canv.setFillColor(style.COLOR_DARK_ORANGE)
            p = Paragraph("<font size=25 name='Titillium-title' color='" + style.COLOR_MEDIUM_ORANGE.hexval() + "'>" + self.text + "</font>", style=getSampleStyleSheet()["Normal"])
            p.wrapOn(self.canv, 460,0.3*inch)
            p.drawOn(self.canv,0,-10)

        self.canv.setStrokeColor(style.COLOR_DARK_ORANGE)
        self.canv.line(0, self.height*-1,460, self.height*-1)

    def wrap(self,*args):
        return (0,self.height)

    def getSpaceAfter(self):
        return self.height+5

#TODO: Even-odd table?
