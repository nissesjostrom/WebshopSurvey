#-!- encoding: utf-8 -!-
#Author: Oscar Arnflo 2016
#Copyright (c) Säkerhetskontoret i Sverige AB 2016

from reportlab.lib.colors import Color
COLOR_DARK_ORANGE = Color(211/255.0,109/255.0,0)
COLOR_MEDIUM_ORANGE = Color(252/255.0,158/255.0,73/255.0)
COLOR_LIGHT_ORANGE = Color(239/255.0,178/255.0,45/255.0)
