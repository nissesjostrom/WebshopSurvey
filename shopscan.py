from reportlib import Report, platypus

from bs4 import BeautifulSoup, SoupStrainer
import urllib2
import sys
import json
import shodan
import nmap
import socket
import dns.resolver
import dns.name
import dns.query
import dns.zone
import gs.dmarc
import PDF
import re


#from Wappalyzer import Wappalyzer, WebPage
from wappalyzer import Wappalyzer
#from pymongo import MongoClient
from urlparse import urlparse
import argparse
NMAP_PORT_RANGE = '0-1000'

def main(argv):

    parser = argparse.ArgumentParser(description='performs a Wappalyzer (https://github.com/AliasIO/Wappalyzer) analysis of the techniques used on the entry site', add_help=True)

    #parser.add_argument('-s', '--start', type=int, default='1', help='Start survey at this prisjakt.nu ftgid. Default = 1')
    #parser.add_argument('-e', '--end', type=int, default='10', help='End survey at this prisjakt.nu ftgid. Default = 10')
    parser.add_argument('-U', '--URL', default=None, help='Host address')
    trace = False
    args = parser.parse_args()
    try:
        query= dns.resolver.query(args.URL, 'A')
        ip =  query[0].to_text()
    except:
	ip = "No A record found"

    try:
        query= dns.resolver.query(args.URL, 'MX')
        mx= query[0].to_text().split()[1]

        print 'MX record: ', mx
        try:
            dmarc =  '%s%s' %('dMARC', re.sub("(.{56})", "\\1\n", gs.dmarc.receiver_policy(mx).to_text(), 0, re.DOTALL))
            print 'DMARC record: ', dmarc
        except:
            dmarc = "No"
    except:
        mx = "No"
	dmarc = "No"

    try:
        query= dns.resolver.query(args.URL, 'SPF')
        print 'SPF:', query[0].to_text()
        spf = '%s%s' %('SPF', re.sub("(.{56})", "\\1\n", query[0].to_text(), 0, re.DOTALL))
    except:
        try:
            query= dns.resolver.query(args.URL, 'TXT')
            print 'TXT', query[0].to_text()

            spf = '%s%s' %('TXT', re.sub("(.{56})", "\\1\n", query[0].to_text(), 0, re.DOTALL))
        except:
            spf = "No"

    SHODAN_API_KEY = 'eUbNrLeAxzXX36zNtCQhkNjLMr1HDkPY'
    api = shodan.Shodan(SHODAN_API_KEY)
    shodan_data = "No"
    shodan_scan = "No"

    # Wrap the request in a try/ except block to catch errors
    try:
        # Search Shodan
        results = api.search(args.URL)

        # Show the results
        print 'Shodan results found: %s' % results['total']
        if results['total'] != 0:
            for result in results['matches']:
                print 'IP: %s' % result['ip_str']
                print result['data']
                shodan_data = shodan_data, result['data']
                print 'Scan results:'
                print api.scan(result['ip_str'])
                shodan_scan = shodan_scan, api.scan(result['ip_str'])
        else:
            shodan_data = "No"
            shodan_scan = "No"
    except shodan.APIError, e:
        print 'Error: %s' % e







    cmd = "http://%s"  %args.URL
    print cmd
    parsed_uri = urlparse(cmd)


    try:

        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
    except:
        pass
    try:
        resp = urllib2.urlopen(cmd, timeout=15)
        soup = BeautifulSoup(resp.read(), 'lxml')

    except urllib2.HTTPError, e:
        print e.reason
        pass
    except urllib2.URLError, e:
        print e.reason
        pass
    except:
        pass
    try:
        print "Probing HTTP headers..."
        server =  resp.info()['Server']
        cookies=resp.info()['Set-Cookie']
        print server
        print cookies
        cookie = True

    except:
        print "No cookie"
	server = "None"
        cookie = False

    try:
        print "Trying HTTP-TRACE method..."
        method = 'TRACE'
        request = urllib2.Request(cmd)
        request.get_method = lambda: method.upper()
        response = urllib2.urlopen(request)
        payload = response.read()
        headers = response.headers
        print "TRACE: Header:", headers
        print "TRACE: Payload", payload
        trace = True
    except urllib2.HTTPError, e:
        print 'TRACE probe failed, reason: ', e.reason
        trace = False

    except urllib2.URLError, e:
        print e.reason
        trace = False

    except:
        print "Trace request unsuccessful"
        trace = False

    try:
        title = soup.title
        print title.string
        tit = title.string
    except:
        tit = 'None'
        pass


    #Wappalyzer
    output = "[]"
    try:
        w = Wappalyzer(cmd)
        output = w.analyze()
    except:
	try:
	    w = Wappalyzer()
	    output = json.dumps(w.analyze(cmd))
	except:
            print "No response from %s" %cmd


    print "Scanning %s, ports:"%ip, "--top-ports 1000"
    nm = nmap.PortScanner()
    nm.scan(args.URL, arguments='--top-ports 1000')
    #print nm.command_line()
    #print nm.scaninfo()
    #nm.all_hosts()
    #print nm.all_hosts()
    hostname = "None"
    protocol = "None"
    ports = []
    try:
        for host in nm.all_hosts():
            print('----------------------------------------------------')
            print('Host : %s (%s)' % (host, nm[host].hostname()))
            hostname =  nm[host].hostname()
            print('State : %s' % nm[host].state())

            for proto in nm[host].all_protocols():
                print('----------')
                print('Protocol : %s' % proto)
                protocol = proto
                lport = nm[host][proto].keys()
                lport.sort()
                ports = lport
                for port in lport:
                    print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))

    except:
        print "nmap error"
        pass

    print hostname, ip, protocol, ports, mx
    shop = {'URL' : cmd, 'Title' : tit, 'Server' : server, 'IP' : ip, 'Hostname' : hostname, 'TRACE-attempt-successful': trace, 'Protocol' : protocol, 'OpenPorts' : str(ports), 'ShodanIndexed' : shodan_data, 'ShodanScan' : shodan_scan, 'MX' : mx, 'SPFRecord' : spf, 'DMARCPolicy': dmarc }
    json_output=json.loads(output)

    i = 1
    for key in json_output:
        value = json_output[key]

        for category in value['categories']:

            pos = "(%s)%s" %(i,category)
            shop[pos] = key

            if len(value['version']) > 0:
                shop[pos] = "%s %s" %(shop[pos], value['version'])
            i +=i


            print shop[pos]
        i=0







    if cookie:
        print cookies
        #shop['Cookies'] = cookies

        if cookies.find('HttpOnly'):
            shop['CookieHttpOnly']='True'
        if cookies.find('HttpsOnly'):
            shop['CookieHttpsOnly']='True'
        if cookies.find('Secure'):
            shop['CookieSecure']='True'

    
    ['None' if v is None else v for v in shop]
    print shop
    #PDF.PDF('{"PHP" : 5.3, "ASP" : 4.0, ".NET" : 1.01}')
    PDF.PDF(json.dumps(shop), args.URL)
        #result = db.shops.insert(shop)


	

if __name__ == "__main__":
    main(sys.argv)


