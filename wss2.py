
from bs4 import BeautifulSoup, SoupStrainer
import urllib2
import sys
import json

#from Wappalyzer import Wappalyzer, WebPage
from wappalyzer import Wappalyzer
from pymongo import MongoClient
from urlparse import urlparse
import argparse


def main(argv):

    parser = argparse.ArgumentParser(description='Webshop technical survey tool. Searches all webshops indexed by the API at prisjakt.nu and performs a Wappalyzer (https://github.com/AliasIO/Wappalyzer) analysis of the techniques used on the entry site', add_help=True)

    parser.add_argument('-s', '--start', type=int, default='1', help='Start survey at this prisjakt.nu ftgid. Default = 1')
    parser.add_argument('-e', '--end', type=int, default='10', help='End survey at this prisjakt.nu ftgid. Default = 10')
    trace = False
    args = parser.parse_args()
    if args.start > args.end:
        print "Negative range: %d - %d" %(args.start,args.end)
        sys.exit([argv])

    elif args.start  or args.end:
        print "Analyzing webshop %d to %d" %(args.start, args.end)
    client = MongoClient()
    db = client.wss7

    for i in range(args.start,args.end):
        cmd = "http://www.prisjakt.nu/redirect.php?ftgid=%d&go=1" %(i)
        try:
            resp = urllib2.urlopen(cmd, timeout=5)
            soup = BeautifulSoup(resp.read(), 'lxml')
        except urllib2.HTTPError, e:
            print e.reason
            pass
        except urllib2.URLError, e:
            print e.reason
            pass
        except socket.timeout as e:

            errno, errstr = sys.exc_info()[:2]
            if errno == socket.timeout:
                print "There was a timeout"
            else:
                print "There was some other socket error"
                print e
            pass


        #wappalyzer = Wappalyzer.latest()




        for link in soup.find_all('a', href=True):
            print "Shop nr %d: %s" %(i,link['href'])
            cmd = link['href']
            parsed_uri = urlparse(cmd)


            try:

                domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
            except:
                break
            try:
                resp = urllib2.urlopen(cmd, timeout=5)
                soup = BeautifulSoup(resp.read(), 'lxml')

            except urllib2.HTTPError, e:
                print e.reason
                break
            except urllib2.URLError, e:
                print e.reason
                break
            except:
                break
            try:
                cookies=resp.info()['Set-Cookie']
                print cookies
                cookie = True

            except:
                print "No cookie"
                cookie = False
                pass

            try:
                method = 'TRACE'
                request = urllib2.Request(cmd)
                request.get_method = lambda: method.upper()
                response = urllib2.urlopen(request)
                payload = response.read()
                headers = response.headers
                print "Header:"
                print headers
                print "Payload"
                print payload
                trace = True
            except urllib2.HTTPError, e:
                print e.reason
                trace = False
                pass
            except urllib2.URLError, e:
                print e.reason
                trace = False
                pass
            except:
                print "Trace request unsuccessful"
                trace = False
                pass

            try:
                title = soup.title
                print title.string
                tit = title.string
            except:
                tit = 'None'
                pass
            try:
                w = Wappalyzer(domain)
                output = w.analyze()


            except:
                print "No response from %s" %cmd
                break
            shop = {'Shop' : domain, 'URL' : cmd, 'Title' : tit, 'ftgid' : i}
            json_output=json.loads(output)


            for key in json_output:
                value = json_output[key]

                for category in value['categories']:

                    shop[category] = [key, value['version'], value['confidence']]
                    print shop[category]








            if cookie:
                print cookie
                cookie = False
                if cookies.find('HttpOnly'):
                    shop['CookieHttpOnly']='True'
                if cookies.find('HttpsOnly'):
                    shop['CookieHttpsOnly']='True'
                if cookies.find('Secure'):
                    shop['CookieSecure']='True'

            if trace:
                shop['TRACE']='True'


            print shop
            result = db.shops.insert(shop)
            if result:
                print "New shop inserted in DB: %s" %shop['Shop']




if __name__ == "__main__":
    main(sys.argv)


